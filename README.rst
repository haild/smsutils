=====
SMS Utils
=====

Features:

* Detect telco from msisdn: i.e, detect_telco("84985347264")
* Parse billing amount from shortcode: i.e, get_billing_from_shortcode ("8033")

Installation
============
    $ pip install -e git+https://bitbucket.org/minhnhb/smsutils.git#egg=smsutils